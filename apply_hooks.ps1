$hooksDistPath = $PSScriptRoot
$currentPath = $hooksDistPath
$rootPath = [System.IO.path]::GetPathRoot($currentPath)
$repoPath = $null
while ($currentPath -ne $rootPath) {
    $possibleGitPath = (Join-Path $currentPath ".git")
    if (Test-Path $possibleGitPath -PathType Container) {
        $repoPath = $currentPath
    }
    $currentPath = (Split-Path -Path $currentPath)
}
if (!$repoPath) {
    Write-Error "Git repository is not initialized"
    Exit
}

$hooksPath = Join-Path $repoPath ".git/hooks"
if (Test-Path $hooksPath)
{
    Remove-Item $hooksPath -Force -Recurse
}
New-Item $hooksPath -ItemType Directory

$distPath = Join-Path $hooksDistPath "dist\*"
Copy-Item $distPath $hooksPath

$location = Get-Location
Set-Location $repoPath
$currentRelativePath = (Resolve-Path $hooksDistPath -Relative)
$currentRelativePath = [io.path]::Combine("", $currentRelativePath) -replace "\\", "/"
Set-Location $location

ForEach ($file in Get-ChildItem $hooksPath -File)
{
    $text = (Get-Content -Path $file.FullName -Raw)
    $text = $text -replace "__hooks_real_path__", $currentRelativePath
    Set-Content $text -path $file.FullName
}